# aws-eks

AWS EKS

## Install helm

tbd

## Deploy Application to EKS with helm

tbd

## Reference

* Deploy a Kubernetes Application https://aws.amazon.com/getting-started/hands-on/deploy-kubernetes-app-amazon-eks/
* Deploy a sample Linux workload https://docs.aws.amazon.com/eks/latest/userguide/sample-deployment.html
* Getting started with the AWS Management Console https://docs.aws.amazon.com/eks/latest/userguide/getting-started-console.html
